package mcltech.buttonMasher;

import mcltech.buttonMasher.gui.ActivePanel;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class ClickBot
{
   private Robot robot;
   private final String filename = "config/config.txt";
   private final String separator = ":::";
   private List<Timer> timers;
   private boolean isRunning = false;
   private Map<Character, int[]> charMap;
   private List<Character> characterOrder;
   private ActivePanel activePanel;

   public ClickBot(ActivePanel activePanel)
   {
      this.activePanel = activePanel;
      robot = null;
      try
      {
         robot = new Robot();
      }
      catch (AWTException e)
      {
         System.out.println("ERROR: Could not initialize Robot");
         e.printStackTrace();
      }
   }

   /**
    * Stop button mashing and clear the config out of memory.
    */
   public void stopRobot()
   {
      System.out.println("Stopping Robot");
      isRunning = false;
      cleanTimers();
   }
   
   /**
    * Load the config file and start executing button mashing. The config re-loads
    * on every start, so it can be updated between start / stop cycles.
    */
   public void startRobot()
   {
      System.out.println("Starting Robot");
      cleanTimers(); // just in case something went wrong and we still have timers
      
      // read the config and load values into char/double map for character / seconds
      readConfig();
      timers = new ArrayList<Timer>();

      isRunning = true;
      restartCycle(0);
   }

   private int addPresses() {
      int currentTotal = 0;
      for (Character c : characterOrder)
      {
         int currentWait = charMap.get(c)[1];
         if (c == null) {
            continue;
         }
         int actualWait = addTimer(c,charMap.get(c)[0],currentWait,currentTotal);
         currentTotal += actualWait;
      }

      return currentTotal;
   }
   
   private void readConfig()
   {
      charMap = new HashMap<>();
      characterOrder = new ArrayList<>();
      try (BufferedReader br = new BufferedReader(new FileReader(filename)))
      {
         String line;
         while ((line = br.readLine()) != null)
         {
            if (!line.contains(separator) || line.startsWith("//") || line.startsWith("#"))
            {
               continue;
            }
            String[] arr = line.split(separator,3);
            if (arr.length != 3)
            {
               System.out.println("ERROR: Config line [" + line + "] is incorrect.");
               System.out.println("       line should be character:::pressSeconds:::delaySeconds... for example, q:::33.5:::2.1");
            }
            // first entry is our randomness entries
            if (!charMap.containsKey(null)) {
               int d1 = (int)(Double.valueOf(arr[1])*1000);
               int d2 = (int)(Double.valueOf(arr[2])*1000);
               int[] vals = {d1,d2};
               charMap.put(null,vals);
               continue;
            }
            char c;
            if (arr[0].equals("\\n")) {
               c = '\n';
            } else {
               c = arr[0].charAt(0);
            }
            int d1 = (int)(Double.valueOf(arr[1])*1000);
            int d2 = (int)(Double.valueOf(arr[2])*1000);
            int[] vals = {d1,d2};
            charMap.put(c,vals);
            characterOrder.add(c);
         }
      }
      catch (IOException e)
      {
         System.out.println("Couldn't read file " + filename);
         e.printStackTrace();
         return;
      }
      catch (NumberFormatException e)
      {
         System.out.println("ERROR: Failed converting a number in the config");
         e.printStackTrace();
         return;
      }
      
      if (charMap.size() == 0)
      {
         System.out.println("ERROR: No config loaded.");
         System.out.println("Config lines should be character:::seconds ... for example, q:::33.5");
         System.out.println("Save in " + filename);
         return;
      }
   }

   private void restartCycle(int msWait) {
      Timer t = new Timer();
      t.schedule(new TimerTask() {
         @Override
         public void run()
         {
            if (isRunning) {
               int totalDelay = addPresses();
               restartCycle(totalDelay);
            }
         }
      }, msWait);
   }
   
   private int addTimer(Character c, int msPress, int msWait, int msPrevious)
   {
      Timer t = new Timer();
      int internalDelay = (int)(Math.random()*charMap.get(null)[1]) + msWait;
      t.schedule(new TimerTask() {
         @Override
         public void run()
         {
            pressKey(c, msPress, charMap.get(null)[0]);
            timers.remove(this);
            if (internalDelay >= 1000) {
               activePanel.startWaiting(internalDelay, false);
            }
         }
      }, msPrevious);
      timers.add(t);
      return internalDelay;
   }
   
   private void pressKey(Character c, int pressms, int randomnessms)
   {
      int keyCode = KeyEvent.getExtendedKeyCodeForChar(c);
      String key = c + "";
      if (c == '\n') {
         key = "<return>";
      }
      System.out.println("Clicking " + key + " [" + keyCode + "]");
      robot.keyPress(keyCode);
      robot.delay(5+(int)(Math.random()*randomnessms) + pressms);
      robot.keyRelease(keyCode);
   }
   
   private void cleanTimers()
   {
      if (timers != null)
      {
         for (Timer t : timers)
         {
            t.cancel();
         }
      }
      timers = new ArrayList<Timer>();
   }
}
